import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import SocketIO from "socket.io-client";
import VueSocketIO from "vue-socket.io";

Vue.config.productionTip = false;

const options = {}; //{ path: "/my-app/" }; //Options object to pass into SocketIO

Vue.use(
  new VueSocketIO({
    debug: true,
    // connection: SocketIO("http://metinseylan.com:1992", options), //options object is Optional
    connection: SocketIO(process.env.VUE_APP_API_URL, options), //options object is Optional
    // connection: SocketIO("https://3000-jade-kiwi-fqw7kkl7.ws-us03.gitpod.io/", options), //options object is Optional
    // vuex: {
    //   store,
    //   actionPrefix: "SOCKET_",
    //   mutationPrefix: "SOCKET_"
    // }
  })
);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
