import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    connect: false,
    name: null,
    rooms: [],
    games: []
  },
  mutations: {},
  actions: {},
  modules: {}
});
