import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/home"
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    props: true
  },
  {
    path: "/intro",
    name: "Intro",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Intro.vue"),
    // props: true
    props: (route) => ({ skip: (route.query.skip == 'true') })
  },
  {
    path: "/game/:name",
    name: "Game",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Game.vue"),
    props: true
  },
  {
    path: "/search",
    name: "search",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Search.vue")
  },
  {
    path: "/welcome",
    name: "welcome",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Welcome.vue")
  },
  {
    path: "/room",
    name: "Room",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Room.vue")
  },
  {
    path: "/credits",
    name: "Credits",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Credits.vue")
  },
  {
    path: "/room/:name",
    name: "Game Room",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GameRoom.vue"),
    props: true
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
