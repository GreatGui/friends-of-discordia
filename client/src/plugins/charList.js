const charList = [
  {
    id: 0,
    name: "Orik Krügner",
    sub: "Giant Dwarf",
    image: require("@/assets/splashPhotos/hurick.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Hurick.png")
  },
  {
    id: 1,
    name: "Bob Gui",
    sub: "Game Creator",
    image: require("@/assets/splashPhotos/gui.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Bob.png")
  },
  {
    id: 2,
    name: "Uéitu Hatorai",
    sub: "Pseudo Hacker",
    image: require("@/assets/splashPhotos/well.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Ueitu.png")
  },
  {
    id: 3,
    name: "Ragevaldo Done",
    sub: "Gamer Office",
    image: require("@/assets/splashPhotos/edu.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Done.png")
  },
  {
    id: 4,
    name: "Mia Aparecida",
    sub: "Recruit",
    image: require("@/assets/splashPhotos/mila.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Mia.png")
  },
  {
    id: 5,
    name: "Téti Retse",
    sub: "Done Love",
    image: require("@/assets/splashPhotos/ester.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Teti.png")
  },
  {
    id: 6,
    name: "Claudinéte Martins",
    sub: "Dead By Daylight",
    image: require("@/assets/splashPhotos/sara.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Sara.png")
  },
  {
    id: 7,
    name: "Cido PlusUltra",
    sub: "Guest",
    image: require("@/assets/splashPhotos/fefa.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Cido-2.png")
  },
  // {
  //   id: 8,
  //   name: "Cometa Estelar",
  //   sub: "Depressive Artist",
  //   image: require("@/assets/splashPhotos/cometa.png"),
  //   sheet: require("@/assets/sheets/WalkingDemo-Cometa.png")
  // },
  {
    id: 8,
    name: "Tibbers",
    sub: "Done Pet",
    image: require("@/assets/splashPhotos/tibbers.png"),
    sheet: require("@/assets/sheets/WalkingDemo-Tibbers.png")
  }
]

export default charList;