import express from 'express'
import http from 'http'
import createGame from './game.js'
import socketIO from 'socket.io'

const app = express()
const server = http.createServer(app)
const sockets = socketIO(server)

app.use(express.static('../client/dist'))

let rooms = []
const games = {}
const removableEvents = [
    "leave-room",
    "update-room",
    "start-room",
    // "revive",
    // "reset",
    "disconnect",
    "move-player",
    "leave-match"
]

sockets.on('connection', (socket) => {
    // console.log("connection", socket)
    const playerId = socket.id

    socket.on('ping-server', (startTime) => {
        console.log(`> ping`, startTime)
        socket.emit('pong-client', startTime)
    });

    socket.on('get-rooms', () => {
        console.log(`> List all Rooms ${rooms.length}`)
        
        socket.emit('all-rooms', rooms);
    });

    socket.on('create-room', ({ room, playerName }) => {
        const roomExists = rooms.find(r => r.name == room.name)
        const slots = [
            { index: 0, confirmed: false },
            { index: 1, confirmed: false },
            { index: 2, confirmed: false },
            { index: 3, confirmed: false },
        ]

        if(roomExists) {
            socket.emit('error-message', 'This room name is already in use')

            return
        }

        socket.join(room.name)
        room.status = "Lobby"
        room.owner = playerId
        room.slots = [...slots]
        room.slots[0] = { ...room.slots[0], ...{ playerId, playerName } }
        rooms.push(room)
        console.log(`> Create Room ${room.name}`)

        socket.emit('created-room', room.name)
    });

    socket.on('try-room', ({ roomName, playerName }) => {
        // console.log(`> List all Rooms ${rooms.length}`)
        const room = rooms.find(room => room.name == roomName)

        if(!room) {
            socket.emit('error-message', 'Room not Found')
        } else if (room.slots && room.slots.filter(slot => !slot.playerId).length == 0) {
            socket.emit('error-message', 'Room is full')
        } else {
            let slotsFree = room.slots.filter(slot => !slot.playerId)
            slotsFree[0].playerId = playerId
            slotsFree[0].playerName = playerName

            socket.join(roomName)
            sockets.to(room.name).emit('updated-room', room.slots)

            socket.emit('enter-room', room.name)
        }
    });

    socket.on('get-room', (roomName) => {
        const room = rooms.find(r => r.name == roomName)

        removableEvents.map(e => socket.removeAllListeners(e))
        // socket.removeAllListeners(removableEvents)

        console.log("sockets", socket._events)

        if(!room) {
            socket.emit('error-message', 'Room not Found')
            return
        }

        const playerExists = room.slots.find(slot => slot.playerId == playerId)

        if(!playerExists) {
            socket.emit('error-message', 'You do not belong in this room')
            socket.leave(roomName)
            return
        }

        socket.emit('game-room', { room, playerId })

        socket.on('leave-room', () => {
            const playerIndex = room.slots.findIndex(slot => slot.playerId == playerId)
            const emptyPlayer = {
                index: playerIndex,
                confirmed: false
            }

            socket.leave(room.name)

            if(playerIndex == -1) {
                socket.emit('error-message', `You has disconnected`)
                return
            }

            if(room.owner == playerId) {
                let index = rooms.findIndex(room => room.name == roomName)
                
                rooms.splice(index, 1)
            } else {
                room.slots.splice(playerIndex, 1, emptyPlayer)
            }

            sockets.to(room.name).emit('updated-room', room.slots)
        });

        socket.on('update-room', (player) => {
            const playerIndex = room.slots.findIndex(slot => slot.playerId == playerId)

            if(playerIndex == -1) {
                sockets.to(room.name).emit('error-message', `${player.playerName} has disconnected`)
                socket.leave(room.name)
                return
            }

            room.slots[playerIndex] = { ...room.slots[playerIndex], ...player }
            // if(playerId != player.playerId) {
            sockets.to(room.name).emit('updated-room', room.slots)
            // }
        });

        socket.on('start-room', () => {
            room.status = "In Game"
            sockets.to(room.name).emit('started-room')

            // games[room.name] = {}
            // game = games[room.name]
            // game = createGame(room)
            // game.start(room.slots)
        });
    });

    socket.on('create-game', (roomName) => {
        const room = rooms.find(room => room.name == roomName)

        const createEvents = ["revive", "reset"]

        createEvents.map(e => socket.removeAllListeners(e))

        setTimeout(() => {
            games[room.name] = createGame(room)
            games[room.name].start(room.slots)

            games[room.name].subscribe((command) => {
                console.log(`> Emitting ${command.type}`)
                sockets.to(roomName).emit(command.type, command)
            })

            sockets.to(roomName).emit('setup', games[room.name].state)

            socket.on('revive', () => {
                console.log(`> Player reset: ${playerId}`)
        
                games[room.name].revivePlayer({ playerId })
            })

            console.log(`> Game criado: `, games[room.name])
        
            socket.on('reset', () => {
                console.log(`> Game reset: `)
        
                games[room.name].resetGame()
            })
        }, 1000)

        // socket.on('restart-match', () => {
        //     game.restart()
        // })
    })

    socket.on('play-game', (roomName) => {
        const game = games[roomName]

        removableEvents.map(e => socket.removeAllListeners(e))
        // console.log('play-game', game, roomName, games)

        socket.on('leave-match', () => {
            const room = rooms.find(room => room.name == roomName)

            if(!room) {
                return
            }

            const playerIndex = room.slots.findIndex(slot => slot.playerId == playerId)
            const emptyPlayer = {
                index: playerIndex,
                confirmed: false
            }

            socket.leave(room.name)

            if(playerIndex == -1) {
                socket.emit('error-message', `You has disconnected`)
                return
            }

            if(room.owner == playerId) {
                let index = rooms.findIndex(room => room.name == roomName)
                
                rooms.splice(index, 1)
            } else {
                room.slots.splice(playerIndex, 1, emptyPlayer)
            }
        });

        socket.on('disconnect', () => {
            const room = rooms.find(room => room.name == roomName)

            game.removePlayer({ playerId })
            console.log(`> Player disconnected: ${playerId}`)

            if(room && room.owner == playerId) {
                let index = rooms.findIndex(room => room.name == roomName)
                
                rooms.splice(index, 1)

                socket.emit('error-message', 'Room is down')
            }
        })

        socket.on('move-player', (command) => {
            command.playerId = playerId
            command.type = 'move-player'

            game.movePlayer(command)
        })
    })
})

server.listen(3000, () => {
    console.log(`> Server listening on port: 3000`)
})
