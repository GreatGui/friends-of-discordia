export default function createGame({ name, owner, width = 9, height = 9, loot = 2, box = 2 }) {
    const lastWidth = width - 1
    const lastHeight = height - 1
    const tileSize = 48

    const state = {
        match: 0,
        roomName: name,
        roomOwner: owner,
        loot,
        box,
        players: {},
        bombs: {},
        screen: {
            tile: tileSize,
            width,
            height,
            maxWidth: tileSize * lastWidth,
            maxHeight: tileSize * lastHeight
        },
        field: [
            // [0, 0, 0, 0, 0, 0, 0, 0, 0],
            // [0, 1, 0, 1, 0, 1, 0, 1, 0],
            // [0, 0, 0, 0, 2, 2, 0, 0, 0],
            // [0, 1, 0, 1, 0, 1, 0, 1, 0],
            // [0, 0, 0, 0, 0, 0, 0, 0, 0],
            // [0, 1, 0, 0, 0, 1, 0, 1, 0],
            // [0, 0, 0, 0, 0, 0, 0, 0, 0],
            // [0, 1, 0, 1, 0, 1, 0, 1, 0],
            // [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]
    }

    //field 2.0 not number
    //object with visual: string, block: nool, palyerID: null
    //object with visual: string, type [block:, visual, colectable ], palyerID: null
    const observers = []
    const body = state.screen.tile - 1

    const reservedSlots = [
        { x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 0 },
        { x: lastWidth, y: lastHeight }, { x: lastWidth - 1, y: lastHeight }, { x: lastWidth, y: lastHeight - 1 },
        { x: 0, y: lastHeight }, { x: 0, y: lastHeight - 1 }, { x: 1, y: lastHeight },
        { x: lastWidth, y: 0 }, { x: lastWidth, y: 1 }, { x: lastWidth - 1, y: 0 }
    ]

    const playerSpawns = [
        { x: 0, y: 0 },
        { x: state.screen.maxWidth, y: state.screen.maxHeight },
        { x: 0, y: state.screen.maxHeight },
        { x: state.screen.maxWidth, y: 0 }
    ]

    const skills = [
        "BombUp",
        "IceMake",
        "InvisibleRat",
        "Summon",
        "",
        "",
        "FireUp",
        "Smash",
        "StealAll",
        "Nothing"
    ]

    function start(players = []) {
        state.field = Array.from({length: height}, e => Array(width).fill(0))
        state.match++

        for(let x = 0; x < width; x++) {
            for(let y = 0; y < height; y++) {
                if(x % 2 && y % 2) {
                    state.field[y][x] = 1
                } else {
                    if (Math.floor(Math.random() * state.box)) {
                        state.field[y][x] = 2
                    }
                }
            }
        }

        reservedSlots.map(slot => {
            state.field[slot.y][slot.x] = 0
        })

        players.map(player => {
            if(player.playerId) {
                addPlayer(player)
            }
        })
    }

    function subscribe(observerFunction) {
        observers.push(observerFunction)
    }

    function notifyAll(command) {
        for (const observerFunction of observers) {
            observerFunction(command)
        }
    }

    function setState(newState) {
        Object.assign(state, newState)
    }

    function addPlayer(command, notify = true) {
        const index = 'index' in command ? command.index : Object.keys(state.players).length
        const spawn = playerSpawns[index]
        const playerId = command.playerId
        // const playerX = 'playerX' in command ? command.playerX : Math.floor(Math.random() * state.screen.maxWidth)
        // const playerY = 'playerY' in command ? command.playerY : Math.floor(Math.random() * state.screen.maxHeight)
        const playerX = spawn.x
        const playerY = spawn.y
        const playerWins = 0
        const playerName = command.playerName
        const id = command.id
        const totalKills = 0
        const kills = 0
        const totalDeaths = 0
        const deaths = 0
        const speed = 16
        const power = 2
        const maxBomb = 2
        const bomb = 2
        const skill = null
        const direction = 'down'
        const idle = true
        const isLive = true

        state.players[playerId] = {
            x: playerX,
            y: playerY,
            playerWins,
            playerName,
            index,
            id,
            totalKills,
            kills,
            totalDeaths,
            deaths,
            speed,
            power,
            maxBomb,
            bomb,
            skill,
            direction,
            idle,
            isLive,
        }

        if(notify) {
            notifyAll({
                type: 'add-player',
                playerId,
                playerX,
                playerY,
                playerWins,
                playerName,
                index,
                id,
                totalKills,
                kills,
                totalDeaths,
                deaths,
                speed,
                power,
                maxBomb,
                bomb,
                skill,
                direction,
                idle,
                isLive
            })
        }
    }

    function removePlayer(command) {
        const playerId = command.playerId

        delete state.players[playerId]

        if(!Object.keys(state.players).length) {

        }

        notifyAll({
            type: 'remove-player',
            playerId
        })
    }

    function resetGame() {
        // const players = Object.values(state.players)

        start();

        for(let playerId in state.players) {
            let player = state.players[playerId]
            const index = 'index' in player ? player.index : Object.keys(state.players).length
            const spawn = playerSpawns[index]

            player.y = spawn.y
            player.x = spawn.x
            player.isLive = true
            player.skill = null
            player.speed = 16
            player.power = 2
            player.maxBomb = 2
            player.bomb = 2
            player.kills = 0
            player.deaths = 0
        }

        notifyAll({
            type: 'reset-game',
            state
        })
    }

    function revivePlayer(command) {
        const playerId = command.playerId

        state.players[playerId].isLive = true

        notifyAll({
            type: 'revive-player',
            playerId
        })
    }

    function movePlayer(command) {
        if(!state.players[command.playerId].isLive) {
            return
        }

        notifyAll(command)

        const acceptedMoves = {
            ArrowUp(player, playerId) {
                const left = { y: player.y - player.speed, x: player.x }
                const right = { y: player.y - player.speed, x: player.x + body }
                player.direction = "up"

                if (player.y - player.speed >= 0) {
                    if (!checkWallCollision(left, right)) {
                        player.y = player.y - player.speed

                        checkCollectable(player, playerId)
                    } else {
                        player.y = Math.trunc(player.y / state.screen.tile) * state.screen.tile
                    }
                } else {
                    player.y = 0
                }
            },
            ArrowRight(player, playerId) {
                const left = { y: player.y, x: player.x + body + player.speed }
                const right = { y: player.y + body, x: player.x + body + player.speed }
                player.direction = "right"

                 if (player.x + player.speed < state.screen.maxWidth) {
                    if (!checkWallCollision(left, right)) {
                        player.x = player.x + player.speed

                        checkCollectable(player, playerId)
                    } else {
                        player.x = Math.trunc((player.x + body) / state.screen.tile) * state.screen.tile
                    }
                } else {
                    player.x = state.screen.maxWidth;
                }
            },
            ArrowDown(player, playerId) {
                const left = { y: player.y + body + player.speed, x: player.x }
                const right = { y: player.y + body + player.speed, x: player.x + body}
                player.direction = "down"

                if (player.y + player.speed < state.screen.maxHeight) {
                    if (!checkWallCollision(left, right)) {
                        player.y = player.y + player.speed

                        checkCollectable(player, playerId)
                    } else {
                        player.y = Math.trunc((player.y + body) / state.screen.tile) * state.screen.tile
                    }
                } else {
                    player.y = state.screen.maxHeight
                }
            },
            ArrowLeft(player, playerId) {
                const left = { y: player.y, x: player.x - player.speed }
                const right = { y: player.y + body, x: player.x - player.speed }
                player.direction = "left"

                 if (player.x - player.speed >= 0) {
                    if (!checkWallCollision(left, right)) {
                        player.x = player.x - player.speed

                        checkCollectable(player, playerId)
                    } else {
                        player.x = Math.trunc(player.x / state.screen.tile) * state.screen.tile
                    }
                } else {
                    player.x = 0
                }
            },
            q(player, playerId) {
                console.log("player.bomb", player.bomb)
                if(player.bomb < 1) {
                    return
                }

                const bomb = { 
                    y: Math.round(player.y / state.screen.tile),
                    x: Math.round(player.x / state.screen.tile)
                }

                if(state.field[bomb.y][bomb.x] > 0) {
                    return
                }

                state.field[bomb.y][bomb.x] = 10
                player.bomb--

                notifyAll({
                    type: 'bomb-planted',
                    playerId,
                    bomb
                })

                setTimeout(() => {
                    console.log("bomb-explode", bomb)

                    explosion(bomb, player, playerId)
                }, 3000)

                console.log("bomb", bomb)
            }
        }

        const keyPressed = command.keyPressed
        const playerId = command.playerId
        const player = state.players[playerId]
        const moveFunction = acceptedMoves[keyPressed]

        if (player && moveFunction) {
            moveFunction(player, playerId)
            checkForFruitCollision(playerId)
        }
    }

    function checkForFruitCollision(playerId) {
        const player = state.players[playerId]

        for (const fruitId in state.fruits) {
            const fruit = state.fruits[fruitId]
            console.log(`Checking ${playerId} and ${fruitId}`)

            if (player.x === fruit.x && player.y === fruit.y) {
                console.log(`COLLISION between ${playerId} and ${fruitId}`)
                removeFruit({ fruitId })
            }
        }
    }

    function checkWallCollision(left, right) {
        let column = Math.trunc(left.x / state.screen.tile)
        let row = Math.trunc(left.y / state.screen.tile)
        // let row = Math.trunc((left.y + 12) / this.state.screen.tile)
  
        console.log("tile left:", row, column, left.x, left.y)
  
        if(state.field[row][column] > 0) {
            return true
        }
  
        column = Math.trunc(right.x / state.screen.tile)
        row = Math.trunc(right.y / state.screen.tile)
  
        console.log("tile right:", row, column, right.x, right.y)
  
        return state.field[row][column] > 0
    }

    function checkCollectable(player, playerId) {
        const PlayerX = Math.round(player.x / state.screen.tile)
        const PlayerY = Math.round(player.y / state.screen.tile)
        const square = state.field[PlayerY][PlayerX]
    
        if(square < 0 && square > -9) {
            typeOfCollectable(square, player)
            state.field[PlayerY][PlayerX] = 0

            console.log("checkCollectable - ", PlayerY, PlayerX, square)

            notifyAll({
                type: 'player-collect',
                playerId,
                player,
                square: { x: PlayerX, y: PlayerY }
            })
        }
    }

    function typeOfCollectable(square, player){
        switch(square) {
            case -1:
                player.power++ 
                break
            case -2:
                player.maxBomb++
                player.bomb < player.maxBomb && player.bomb++
                break
            // case -5:
            //     player.maxBomb++
            //     player.bomb < player.maxBomb && player.bomb++
            //     break
        }

        square = 0
    }

    function drawFire(fireSquares, lastFire, middleFire) {
        if(fireSquares.length) {
            const first = fireSquares.pop()
    
            state.field[first.y][first.x] = lastFire
    
            fireSquares.map(fire => state.field[fire.y][fire.x] = middleFire)
        }
    }

    function bombKill(normalSquares, BomberId) {
        for(const playerId in state.players) {
            const player = state.players[playerId]
            const PlayerX = Math.round(player.x / state.screen.tile)
            const PlayerY = Math.round(player.y / state.screen.tile)

            if(!player.isLive) {
                continue
            }

            normalSquares.map(square => {
                if(PlayerX == square.x && PlayerY == square.y) {
                    state.players[playerId].isLive = false
                    state.players[playerId].deaths++
                    state.players[playerId].totalDeaths++

                    if(BomberId != playerId) {
                        state.players[BomberId].kills++
                        state.players[BomberId].totalKills++
                    }

                    gameWin()

                    notifyAll({
                        type: 'bomb-kill',
                        playerId,
                        players: state.players
                    })
                }
            })
        }
    }

    function explosion(bomb, player, playerId) {
        const power = player.power
        const fireCenter = { y: bomb.y, x: bomb.x }
        let fireUp = []
        let fireDown = []
        let fireRight = []
        let fireLeft = []
        let breakBox = []
        
        for(let i = 1; i <= power; i++) {
          let val = bomb.y - i

          if(val < 0 || state.field[val][bomb.x] == 1) {
            break
          }

          if(state.field[val][bomb.x] == 2) {
            breakBox.push({ y: val, x: bomb.x })
            fireUp.push({ y: val, x: bomb.x })
            break
          }

          fireUp.push({ y: val, x: bomb.x })
        }

        for(let i = 1; i <= power; i++) {
          let val = bomb.y + i

          if(val >= state.screen.height || state.field[val][bomb.x] == 1) {
            break
          }

          if(state.field[val][bomb.x] == 2) {
            breakBox.push({ y: val, x: bomb.x })
            fireDown.push({ y: val, x: bomb.x })
            break
          }

          fireDown.push({ y: val, x: bomb.x })
        }

        for(let i = 1; i <= power; i++) {
          let val = bomb.x + i

          if(val >= state.screen.width || state.field[bomb.y][val] == 1) {
            break
          }

          if(state.field[bomb.y][val] == 2) {
            breakBox.push({ y: bomb.y, x: val })
            fireRight.push({ y: bomb.y, x: val })
            break
          }

          fireRight.push({ y: bomb.y, x: val })
        }

        for(let i = 1; i <= power; i++) {
          let val = bomb.x - i

          if(val < 0 || state.field[bomb.y][val] == 1) {
            break
          }

          if(state.field[bomb.y][val] == 2) {
            breakBox.push({ y: bomb.y, x: val })
            fireLeft.push({ y: bomb.y, x: val })
            break
          }

          fireLeft.push({ y: bomb.y, x: val })
        }

        const normalSquares = [
            fireCenter,
            ...fireUp,
            ...fireDown,
            ...fireRight,
            ...fireLeft
        ]
        
        notifyAll({
            type: 'bomb-fire',
            playerId,
            squares: {
                fireCenter,
                fireUp,
                fireDown,
                fireRight,
                fireLeft
            }
        })

        state.field[fireCenter.y][fireCenter.x] = -10
        drawFire(fireUp, -13, -11)
        drawFire(fireDown, -14, -11)
        drawFire(fireRight, -15, -12)
        drawFire(fireLeft, -16, -12)
        
        bombKill(normalSquares, playerId)
    
        player.bomb < player.maxBomb && player.bomb++

        setTimeout(() => {
            bombKill(normalSquares, playerId)

            normalSquares.map(square => {
                if(state.field[square.y][square.x] < 0) {
                    state.field[square.y][square.x] = 0
                }
            })

            breakBox.map(square => {
                if(Math.floor(Math.random() * state.loot) == 0) {
                    state.field[square.y][square.x] = Math.floor(Math.random() * -2)
                }
            })

            notifyAll({
                type: 'extinguished-fire',
                field: state.field
            })

        }, 500)
    }

    function gameWin() {
        const playerIds = Object.keys(state.players)
        const livePlayers = playerIds.filter(playerId => state.players[playerId].isLive)
        console.log(livePlayers)

        if(livePlayers.length > 1) {
            return
        }

        if(livePlayers.length == 1) {
            const playerId = livePlayers[0]

            state.players[playerId].playerWins++

            notifyAll({
                type: 'win-match',
                playerId
            })
        } else {
            notifyAll({
                type: 'draw-match',
            })
        }
    }

    // function restart() {
    //     state.field = Array.from({length: height}, e => Array(width).fill(0))
    //     state.match++

    //     for(let x = 0; x < width; x++) {
    //         for(let y = 0; y < height; y++) {
    //             if(x % 2 && y % 2) {
    //                 state.field[y][x] = 1
    //             } else {
    //                 if (Math.floor(Math.random() * state.box)) {
    //                     state.field[y][x] = 2
    //                 }
    //             }
    //         }
    //     }

    //     reservedSlots.map(slot => {
    //         state.field[slot.y][slot.x] = 0
    //     })

    //     notifyAll({
    //         type: 'reset-game',
    //         state
    //     })
    // }

    return {
        addPlayer,
        removePlayer,
        movePlayer,
        revivePlayer,
        resetGame,
        state,
        setState,
        subscribe,
        start
    }
}
